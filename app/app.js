

var app = angular.module('app', [
  'ngRoute',
  'webcam'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "app/components/home/home.html", controller: "home"})
    // else 404
    .otherwise("/404", {templateUrl: "app/shared/404.html"});
}]);

