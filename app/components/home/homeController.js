app.controller('home', function ($scope, fileUpload) {
    var _video = null;
    $scope.patData = "";
    $scope.displayTakePicture = false;
    $scope.patOpts = {x: 0, y: 0, w: 25, h: 25};

    // Setup a channel to receive a video property
    // with a reference to the video element
    // See the HTML binding in main.html
    $scope.channel = {};
    $scope.imageData = '';
    $scope.webcamError = false;
    $scope.onError = function (err) {
        $scope.$apply(
            function() {
                console.log("error");
                $scope.webcamError = err;
            }
        );
    };

    $scope.onSuccess = function () {
        // The video element contains the captured camera data
        _video = $scope.channel.video;
        $scope.$apply(function() {
            $scope.patOpts.w = _video.width;
            $scope.patOpts.h = _video.height;
            //$scope.showDemos = true;
            $scope.displayTakePicture = true;
        });
    };

    $scope.onStream = function (stream) {
        // You could do something manually with the stream.
    };
   
	  $scope.makeSnapshot = function() {
        if (_video) {
            var patCanvas = document.querySelector('#snapshot');
            if (!patCanvas) return;

            patCanvas.width = _video.width;
            patCanvas.height = _video.height;
            var ctxPat = patCanvas.getContext('2d');

            var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
            ctxPat.putImageData(idata, 0, 0);

            sendSnapshotToServer(patCanvas.toDataURL());

            $scope.patData = patCanvas.toDataURL();
            document.getElementById("image").src = $scope.patData;
            $scope.cropStart();
        }
    };
    
    /**
     * Redirect the browser to the URL given.
     * Used to download the image by passing a dataURL string
     */
    $scope.downloadSnapshot = function downloadSnapshot(dataURL) {
        window.location.href = dataURL;
    };
    
    var getVideoData = function getVideoData(x, y, w, h) {
        var hiddenCanvas = document.createElement('canvas');
        hiddenCanvas.width = _video.width;
        hiddenCanvas.height = _video.height;
        var ctx = hiddenCanvas.getContext('2d');
        ctx.drawImage(_video, 0, 0, _video.width, _video.height);
        return ctx.getImageData(x, y, w, h);
    };

    /**
     * This function could be used to send the image data
     * to a backend server that expects base64 encoded images.
     *
     * In this example, we simply store it in the scope for display.
     */
    var sendSnapshotToServer = function sendSnapshotToServer(imgBase64) {
        $scope.snapshotData = imgBase64;
    };

    // var Cropper = window.Cropper;
    document.getElementById("image").src = $scope.patData;
    $scope.cropStart = function (){
    	var URL = window.URL || window.webkitURL;
    	  var container = document.querySelector('.img-container');
    	  var image = container.getElementsByTagName('img').item(0);
    	  var download = document.getElementById('download');
    	  var actions = document.getElementById('actions');
    	  var dataX = document.getElementById('dataX');
    	  var dataY = document.getElementById('dataY');
    	  var dataHeight = document.getElementById('dataHeight');
    	  var dataWidth = document.getElementById('dataWidth');
    	  var dataRotate = document.getElementById('dataRotate');
    	  var dataScaleX = document.getElementById('dataScaleX');
    	  var dataScaleY = document.getElementById('dataScaleY');
    	  var options = {
    	    aspectRatio: 16 / 9,
    	    preview: '.img-preview',
    	    ready: function (e) {
    	      console.log(e.type);
    	    },
    	    cropstart: function (e) {
    	      console.log(e.type, e.detail.action);
    	    },
    	    cropmove: function (e) {
    	      console.log(e.type, e.detail.action);
    	    },
    	    cropend: function (e) {
    	      console.log(e.type, e.detail.action);
    	    },
    	    crop: function (e) {
    	      var data = e.detail;

    	      console.log(e.type);
    	      dataX.value = Math.round(data.x);
    	      dataY.value = Math.round(data.y);
    	      dataHeight.value = Math.round(data.height);
    	      dataWidth.value = Math.round(data.width);
    	      dataRotate.value = typeof data.rotate !== 'undefined' ? data.rotate : '';
    	      dataScaleX.value = typeof data.scaleX !== 'undefined' ? data.scaleX : '';
    	      dataScaleY.value = typeof data.scaleY !== 'undefined' ? data.scaleY : '';
    	    },
    	    zoom: function (e) {
    	      console.log(e.type, e.detail.ratio);
    	    }
    	  };
    	      cropper = new Cropper(image, options);
    	  var originalImageURL = $scope.patData;
    	  var uploadedImageType = 'image/jpeg';
    	  var uploadedImageName = 'image.jpg';
    	  var uploadedImageURL;

    	  // Tooltip
    	  $('[data-toggle="tooltip"]').tooltip();

    	  // Buttons
    	  if (!document.createElement('canvas').getContext) {
    	    $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
    	  }

    	  if (typeof document.createElement('cropper').style.transition === 'undefined') {
    	    $('button[data-method="rotate"]').prop('disabled', true);
    	    $('button[data-method="scale"]').prop('disabled', true);
    	  }

    	  // Download
    	  if (typeof download.download === 'undefined') {
    	    download.className += ' disabled';
    	    download.title = 'Your browser does not support download';
    	  }

    	  // Options
    	  actions.querySelector('.docs-toggles').onchange = function (event) {
    	    console.log("onchange docs");
    	    var e = event || window.event;
    	    var target = e.target || e.srcElement;
    	    var cropBoxData;
    	    var canvasData;
    	    var isCheckbox;
    	    var isRadio;

    	    if (!cropper) {
    	      return;
    	    }

    	    if (target.tagName.toLowerCase() === 'label') {
    	      target = target.querySelector('input');
    	    }

    	    isCheckbox = target.type === 'checkbox';
    	    isRadio = target.type === 'radio';

    	    if (isCheckbox || isRadio) {
    	      if (isCheckbox) {
    	        options[target.name] = target.checked;
    	        cropBoxData = cropper.getCropBoxData();
    	        canvasData = cropper.getCanvasData();

    	        options.ready = function () {
    	          console.log('ready');
    	          cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
    	        };
    	      } else {
    	        options[target.name] = target.value;
    	        options.ready = function () {
    	          console.log('ready');
    	        };
    	      }

    	      // Restart
    	      cropper.destroy();
    	      cropper = new Cropper(image, options);
    	    }
    	  };

    	  // Methods
    	  actions.querySelector('.docs-buttons').onclick = function (event) {
    	    var e = event || window.event;
    	    console.log("onchange buttons");
    	    var target = e.target || e.srcElement;
    	    var cropped;
    	    var result;
    	    var input;
    	    var data;

    	    if (!cropper) {
    	      return;
    	    }

    	    while (target !== this) {
    	      if (target.getAttribute('data-method')) {
    	        break;
    	      }

    	      target = target.parentNode;
    	    }

    	    if (target === this || target.disabled || target.className.indexOf('disabled') > -1) {
    	      return;
    	    }

    	    data = {
    	      method: target.getAttribute('data-method'),
    	      target: target.getAttribute('data-target'),
    	      option: target.getAttribute('data-option') || undefined,
    	      secondOption: target.getAttribute('data-second-option') || undefined
    	    };

    	    cropped = cropper.cropped;

    	    if (data.method) {
    	      if (typeof data.target !== 'undefined') {
    	        input = document.querySelector(data.target);

    	        if (!target.hasAttribute('data-option') && data.target && input) {
    	          try {
    	            data.option = JSON.parse(input.value);
    	          } catch (e) {
    	            console.log(e.message);
    	          }
    	        }
    	      }

    	      switch (data.method) {
    	        case 'rotate':
    	          if (cropped && options.viewMode > 0) {
    	            cropper.clear();
    	          }

    	          break;

    	        case 'getCroppedCanvas':
    	          try {
    	            data.option = JSON.parse(data.option);
    	          } catch (e) {
    	            console.log(e.message);
    	          }

    	          if (uploadedImageType === 'image/jpeg') {
    	            if (!data.option) {
    	              data.option = {};
    	            }

    	            data.option.fillColor = '#fff';
    	          }

    	          break;
    	      }

    	      result = cropper[data.method](data.option, data.secondOption);
    	      $scope.dataImage = result;
    	      switch (data.method) {
    	        case 'rotate':
    	          if (cropped && options.viewMode > 0) {
    	            cropper.crop();
    	          }

    	          break;

    	        case 'scaleX':
    	        case 'scaleY':
    	          target.setAttribute('data-option', -data.option);
    	          break;

    	        case 'getCroppedCanvas':
    	          if (result) {
    	            // Bootstrap's Modal
    	            $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

    	            if (!download.disabled) {
    	              download.download = uploadedImageName;
    	              download.href = result.toDataURL(uploadedImageType);
    	            }
    	          }

    	          break;

    	        case 'destroy':
    	          cropper = null;

    	          if (uploadedImageURL) {
    	            URL.revokeObjectURL(uploadedImageURL);
    	            uploadedImageURL = '';
    	            $scope.patData = originalImageURL;
    	          }

    	          break;
    	      }

    	      if (typeof result === 'object' && result !== cropper && input) {
    	        try {
    	          input.value = JSON.stringify(result);
    	        } catch (e) {
    	          console.log(e.message);
    	        }
    	      }
    	    }
    	  };

    	  document.body.onkeydown = function (event) {
    	    var e = event || window.event;
    	    console.log("onkeydown");
    	    if (e.target !== this || !cropper || this.scrollTop > 300) {
    	      return;
    	    }

    	    switch (e.keyCode) {
    	      case 37:
    	        e.preventDefault();
    	        cropper.move(-1, 0);
    	        break;

    	      case 38:
    	        e.preventDefault();
    	        cropper.move(0, -1);
    	        break;

    	      case 39:
    	        e.preventDefault();
    	        cropper.move(1, 0);
    	        break;

    	      case 40:
    	        e.preventDefault();
    	        cropper.move(0, 1);
    	        break;
    	    }
    	  };

    	  // Import image
    	  var inputImage = document.getElementById('inputImage');

    	  if (URL) {
    	    inputImage.onchange = function () {
    	      var files = this.files;
    	      var file;

    	      if (cropper && files && files.length) {
    	        file = files[0];

    	        if (/^image\/\w+/.test(file.type)) {
    	          uploadedImageType = file.type;
    	          uploadedImageName = file.name;

    	          if (uploadedImageURL) {
    	            URL.revokeObjectURL(uploadedImageURL);
    	          }

    	          $scope.patData = uploadedImageURL = URL.createObjectURL(file);
    	          cropper.destroy();
    	          cropper = new Cropper(image, options);
    	          inputImage.value = null;
    	        } else {
    	          window.alert('Please choose an image file.');
    	        }
    	      }
    	    };
    	  } else {
    	    inputImage.disabled = true;
    	    inputImage.parentNode.className += ' disabled';
    	  }
    }
  

    $scope.uploadFile = function() {
      if(!$scope.dataImage){
        alert("Error upload");
        return;
      }
      var file = $scope.dataImage.toDataURL(uploadedImageType);
      console.log('file is ' );
      console.dir(file);
      var uploadUrl = "/fileUpload";
      fileUpload.uploadFileToUrl(file, uploadUrl);
    };
});
  
