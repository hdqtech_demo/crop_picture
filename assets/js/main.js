
import homeController from './components/home/homeController';

var app = angular.module('app', [
  'ngRoute'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "app/components/home/home.html", controller: "home"})
    // else 404
    .otherwise("/404", {templateUrl: "app/shared/404.html", controller: "PageCtrl"});
}]);

/**
 * Controls all other Pages
 */
// app.controller('home', function (/* $scope, $location, $http */) {
//   console.log("Page Controller reporting for duty.");

//   // Activates the Carousel
//   $('.carousel').carousel({
//     interval: 5000
//   });

//   // Activates Tooltips for Social Links
//   $('.tooltip-social').tooltip({
//     selector: "a[data-toggle=tooltip]"
//   })
// });
